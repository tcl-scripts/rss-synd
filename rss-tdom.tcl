namespace eval rss {

   set feed(eggheads) {
      "url" "https://forum.eggheads.org/app.php/feed/topics"
      "channel" "#test"
      "database" "eggheads.db"
      "update-interval" 5
      "trigger" "!@@feedid@@"
      "output" "@@title@@ - @@link@@"
   }

   set feed(eggdrop) {
      "url" "https://forum.eggdrop.fr/syndication.php?limit=15"
      "channel" "#test"
      "database" "eggdrop.db"
      "update-interval" 5
      "trigger" "!@@feedid@@"
      "output" "@@title@@ - @@link@@"
   }
}

namespace eval rss {

   set default {
		"announce-output"	3
		"trigger-output"	3
		"remove-empty"		1
		"trigger-type"		0:2
		"announce-type"		0
		"max-depth"			5
		"update-interval"	5
		"output-order"		0
		"timeout"			5000
		"channels"			"#actu"
		"trigger"			"!rss @@feedid@@"
		"output"			"\[\002@@channel!title@@@@title@@\002\] @@item!title@@@@entry!title@@ - @@item!link@@@@entry!link!=href@@"
		"user-agent"		"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11"
		"evaluate-tcl"		1
	}

   package require http
   package require tls
   package require tdom
   
   variable fdatas
   variable lastitem
   
   proc feedget { feed } {
      array set myfeed $::rss::default
      array set myfeed $::rss::feed($feed)
      set myfeed(name) $feed
      ::http::register https 443 [list ::tls::socket -autoservername true]
      ::http::config -useragent $myfeed(user-agent)
      ::http::geturl $myfeed(url) -command "::rss::feedcb {[array get myfeed] depth 0}" -timeout $myfeed(timeout)
   }
   
   # Callback after geturl
   proc feedcb { feed args } {
      set tok [lindex $args end]
      array set myfeed $feed
      upvar 0 $tok state
      if {$state(status) ne "ok"} {
         putlog "HTTP Error : $state(url) made $state(status) error"
         return 1
      }
      array set meta $state(meta)
      if {[::http::ncode $tok] in [list 301 302]} {
         incr myfeed(depth)
         if {$myfeed(depth)<$myfeed(max-depth)} {
            ::http::geturl $myfeed(url) -command "::rss::feedcb {$myfeed}" -timeout $myfeed(timeout)
         } else {
            putlog "HTTP Error : Too much redirections ($myfeed(depth))"
         }
         return 1
      } elseif {[::http::ncode $tok] != 200} {
         putlog "HTTP Error : $state(url) made $state(http) error"
         return 1
      }
      putlog "Got result ($state(http))"
      set fdatas [::http::data $tok]
      ::http::cleanup $tok
      set titem "entry"
      set document [dom parse -ignorexmlns $fdatas]
      set root [$document documentElement]
      set rnode [$root nodeName]
      if { $rnode ne "feed" } {
         set rnode "rss/channel"
         set titem "item"
      }
      set ns [$root namespace]
      foreach node [$root selectNodes /${rnode}/${titem}] {
         foreach child [$node childNodes] {
            switch -nocase -- [$child nodeName] {
               link {
                  if {$rnode eq "feed"} {
                     set entry([$child nodeName]) [$child getAttribute href]
                  } else {
                     set entry([$child nodeName]) [$child asText]
                  }
               }
               category {
                  set entry(channel) [$child getAttribute label]
               }
               title {
                  set entry([$child nodeName]) [$child asText]
               }
               pubDate {
                  set entry([$child nodeName]) [clock scan [$child asText] -format "%a, %d %b %Y %T %Z"]
               }
               published {
                  set entry(pubDate) [clock scan [$child asText] -format "%Y-%m-%dT%T%Z"]
               }
               default { continue }
            }
         }
         # create a list and order it by ts
         if {$entry(pubDate) > $::lastitem($myfeed(name))} {
            ::rss:write2db $myfeed(name) $entry(puDate) $entry(title) $entry(link)
            putlog "$myfeed(channel) : $entry(title) - [clock format $entry(pubDate)] : $entry(link)"
            set ::lastitem($myfeed(name)) $entry(pubDate)
         }
         unset entry
      }
   }
   
   proc cronrun { min hour day month dow } {
      foreach cfeed [array names ::feed] {
         array set tmp $::feed($cfeed)
         if {![file exists $tmp(database)] || ([expr {[unixtime] - [file mtime $tmp(database)]}] > [expr {$tmp(update-interval) * 60}])} {
            ::rss::feedget $cfeed
         }
         
      }
   }
   
   proc write2db { feed ts title link } {
      array set tmp $::feed($feed)
      set fo [open $tmp(database) a]
      puts $fo [list $ts $title $link]
      close $fo
   }
   
   proc init {} {
      foreach cfeed [array names ::feed] {
         array set tmp $::feed($cfeed)
         if {[file exists $tmp(database)]} {
            set ::lastitem($cfeed) 0
            set fi [open $tmp(database) r]
            set datas [split [read -nonewline $fi] "\n"]
            close $fi
            foreach cdata $datas {
               lassign $datas ts
               if {$ts > $::lastitem($cfeed)} {
                  set ::lastitem($cfeed) $ts
               }
            }
         }
      }
   }
}


putlog "*** RSS ***"
# ::rss::feedget eggdrop
putlog "*** atom ***"
# ::rss::feedget eggheads
